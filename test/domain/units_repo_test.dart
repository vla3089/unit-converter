import 'package:flutter_test/flutter_test.dart';
import 'package:unit_converter/domain/units_repo.dart';
import 'package:unit_converter/domain/temperature.dart';

@TestOn('vm')
void main() {
  final unitManager = UnitsRepo();

  test("should have null values until a value is set", () {
    double actual = unitManager.getValueFor(Celsius);
    expect(actual, null);
  });

  test("should have values when temperature is set", () {
    unitManager.setValue(Celsius, 0);
    expect(unitManager.getValueFor(Celsius), 0);
    expect(unitManager.getValueFor(Fahrenheit), 32);
  });
}
