@TestOn('vm')

import 'package:test/test.dart';
import 'package:unit_converter/domain/unit_converter.dart';
import 'package:unit_converter/domain/weight.dart';

void main() {
  final converter = UnitConverter();

  final valuesSource = [
    Input(Kg, 1, Kg, 1),
    Input(Kg, 1, G, 1E3),
    Input(Kg, 1, Mg, 1E6),
    Input(Kg, 1, LbPound, 2.2046),
    Input(Kg, 1, Oz, 35.273962),
    Input(LbPound, 1, Kg, 0.453592),
    Input(LbPound, 1, G, 453.59237),
    Input(LbPound, 1, Mg, 453592.37),
    Input(LbPound, 1, LbPound, 1),
    Input(LbPound, 1, Oz, 16),
  ];

  group('Should convert items', () {
    valuesSource.forEach((element) {
      test("${element.a}(${element.valueA}) -> ${element.b}(${element.valueB})",
          () {
        final actual = converter.convert(element.a, element.b, element.valueA);
        expect(actual, element.valueB);
      });

      test("${element.b}(${element.valueB}) -> ${element.a}(${element.valueA})",
          () {
        final actual = converter.convert(element.b, element.a, element.valueB);
        expect(actual, element.valueA);
      });
    });
  });
}

// class for test input data
// expectation is tests verify a -> b, and b -> a convertion
class Input {
  Type a;
  Type b;
  double valueA;
  double valueB;

  Input(this.a, this.valueA, this.b, this.valueB);
}
