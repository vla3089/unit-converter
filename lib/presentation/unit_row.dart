import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:unit_converter/presentation/unit_cell.dart';

class UnitRow extends StatelessWidget {
  final int index;
  final Type left;
  final Type right;
  final bool isInEditMode;
  final VoidCallback onDeletePressed;

  const UnitRow({
    Key key,
    @required this.index,
    @required this.left,
    @required this.right,
    this.isInEditMode = false,
    this.onDeletePressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final children = [
      UnitCell(unitType: left),
      new Container(
          margin: new EdgeInsets.symmetric(horizontal: 4.0),
          child: new Icon(Icons.chevron_left)),
      new Container(
          margin: new EdgeInsets.symmetric(horizontal: 4.0),
          child: new Icon(Icons.chevron_right)),
      UnitCell(unitType: right),
    ];

    if (isInEditMode) {
      children.insert(
          0,
          ReorderableDragStartListener(
            index: index,
            child:
                SizedBox(width: 48, height: 48, child: Icon(Icons.drag_handle)),
          ));
      children.add(SizedBox(
        width: 48,
        height: 48,
        child: new IconButton(
          icon: new Icon(
            Icons.delete_forever_outlined,
            color: Theme.of(context).errorColor,
          ),
          onPressed: onDeletePressed,
        ),
      ));
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(children: children),
    );
  }
}
