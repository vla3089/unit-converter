import 'package:flutter/material.dart';
import 'package:unit_converter/repo/settings_adapter.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ThemeModeRow extends StatelessWidget {
  final SettingsAdapter adapter;
  final Color accentColor;

  const ThemeModeRow({Key key, this.adapter, this.accentColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(AppLocalizations.of(context).theme_mode),
      subtitle: Text(
        _themeModeToString(context, adapter.themeMode),
        style: TextStyle(color: accentColor),
      ),
      onTap: () {
        showModalBottomSheet(
            context: context,
            builder: (context) => _getSelectThemeModeWidget(context));
      },
    );
  }

  Widget _getSelectThemeModeWidget(BuildContext context) {
    final children = ThemeMode.values.map((item) {
      Widget trailing;
      if (item == adapter.themeMode) {
        trailing = Icon(Icons.check, color: accentColor);
      }
      return ListTile(
        leading: Text(_themeModeToString(context, item)),
        trailing: trailing,
        onTap: () {
          adapter.themeMode = item;
          Navigator.of(context).pop();
        },
      );
    }).toList();
    return Container(child: Wrap(children: children));
  }

  String _themeModeToString(BuildContext context, ThemeMode mode) {
    if (mode == ThemeMode.dark) {
      return AppLocalizations.of(context).theme_mode_dark;
    } else if (mode == ThemeMode.light) {
      return AppLocalizations.of(context).theme_mode_light;
    } else if (mode == ThemeMode.system) {
      return AppLocalizations.of(context).theme_mode_system;
    }
    return mode.toString();
  }
}
