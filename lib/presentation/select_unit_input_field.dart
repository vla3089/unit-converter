import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class SelectUnitInputField extends StatefulWidget {
  final String label;
  final Function onChanged;
  final SuggestionsCallback<String> suggestionsCallback;
  final FormFieldValidator<String> validator;

  const SelectUnitInputField({
    Key key,
    this.label,
    this.onChanged,
    this.suggestionsCallback,
    this.validator,
  }) : super(key: key);

  @override
  _SelectUnitInputFieldState createState() => _SelectUnitInputFieldState();
}

class _SelectUnitInputFieldState extends State<SelectUnitInputField> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.addListener(_updateValue);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.removeListener(_updateValue);
  }

  @override
  Widget build(BuildContext context) {
    return TypeAheadFormField<String>(
      hideOnLoading: true,
      textFieldConfiguration: TextFieldConfiguration(
        controller: _controller,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: widget.label,
        ),
      ),
      debounceDuration: Duration(milliseconds: 50),
      suggestionsCallback: widget.suggestionsCallback,
      itemBuilder: (context, suggestion) {
        return ListTile(
          title: Text(suggestion),
        );
      },
      onSuggestionSelected: (suggestion) {
        this._controller.text = suggestion;
      },
      validator: widget.validator,
    );
  }

  _updateValue() {
    widget.onChanged(_controller.text);
  }
}
