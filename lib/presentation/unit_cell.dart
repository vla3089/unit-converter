import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:unit_converter/domain/units_repo.dart';
import 'package:provider/provider.dart';
import 'package:unit_converter/presentation/titles_factory.dart';

class UnitCell extends StatefulWidget {
  // unitType = Celcius, Farehnheit, lb...
  final Type unitType;

  const UnitCell({Key key, this.unitType}) : super(key: key);

  @override
  _UnitCellState createState() => _UnitCellState();
}

class _UnitCellState extends State<UnitCell> {
  final _controller = TextEditingController();
  final _focusNode = FocusNode();

  UnitsRepo model;
  TitlesFactory titlesFactory;

  @override
  void initState() {
    super.initState();
    _controller.addListener(_updateValue);
    model = context.read<UnitsRepo>();
    titlesFactory = context.read<TitlesFactory>();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final value = context.select(
      (UnitsRepo model) => model.getValueFor(
        widget.unitType,
      ),
    );

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (_focusNode.hasFocus == false) {
        _controller.removeListener(_updateValue);
        if (value == null) {
          _controller.text = "";
        } else {
          _controller.text = value.toStringAsFixed(2);
        }
        _controller.addListener(_updateValue);
      }
    });

    return Expanded(
      child: TextField(
        keyboardType: TextInputType.numberWithOptions(decimal: true),
        focusNode: _focusNode,
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
          LengthLimitingTextInputFormatter(16)
        ],
        controller: _controller,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: titlesFactory.getTitle(context, widget.unitType),
        ),
      ),
    );
  }

  _updateValue() {
    double value = double.tryParse(_controller.text);
    model.setValue(widget.unitType, value);
  }
}
