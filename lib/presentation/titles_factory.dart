import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:unit_converter/domain/area.dart';
import 'package:unit_converter/domain/speed.dart';
import 'package:unit_converter/domain/temperature.dart';
import 'package:unit_converter/domain/weight.dart';

class TitlesFactory with ChangeNotifier {
  String getTitle<Type>(BuildContext context, Type type) {
    if (type == Celsius().runtimeType) {
      return AppLocalizations.of(context).celcius;
    } else if (type == Fahrenheit().runtimeType) {
      return AppLocalizations.of(context).fahrenheit;
    } else if (type == Kg().runtimeType) {
      return AppLocalizations.of(context).kg;
    } else if (type == G().runtimeType) {
      return AppLocalizations.of(context).g;
    } else if (type == Mg().runtimeType) {
      return AppLocalizations.of(context).mg;
    } else if (type == Oz().runtimeType) {
      return AppLocalizations.of(context).oz;
    } else if (type == LbPound().runtimeType) {
      return AppLocalizations.of(context).lbPound;
    } else if (type == SqF().runtimeType) {
      return AppLocalizations.of(context).sqF;
    } else if (type == SqM().runtimeType) {
      return AppLocalizations.of(context).sqM;
    } else if (type == Mph().runtimeType) {
      return AppLocalizations.of(context).mph;
    } else if (type == KmPerHour().runtimeType) {
      return AppLocalizations.of(context).kmph;
    }

    throw Exception("Unknown type: $type");
  }
}
