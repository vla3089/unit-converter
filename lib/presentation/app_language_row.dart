import 'package:flutter/material.dart';
import 'package:unit_converter/repo/settings_adapter.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AppLanguageRow extends StatelessWidget {
  final SettingsAdapter adapter;
  final Color accentColor;

  const AppLanguageRow({Key key, this.adapter, this.accentColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currLocale = adapter.locale ?? Localizations.localeOf(context);

    return ListTile(
      title: Text(AppLocalizations.of(context).app_language),
      subtitle: Text(
        currLocale.toLanguageTag(),
        style: TextStyle(color: accentColor),
      ),
      onTap: () {
        showModalBottomSheet(
            context: context,
            builder: (context) =>
                _getSelectLocaleWidget(context, adapter, currLocale));
      },
    );
  }

  Widget _getSelectLocaleWidget(
    BuildContext context,
    SettingsAdapter adapter,
    Locale currLocale,
  ) {
    final children = AppLocalizations.supportedLocales.map((item) {
      Widget trailing;
      if (item == currLocale) {
        trailing = Icon(Icons.check, color: accentColor);
      }
      return ListTile(
        leading: Text(item.languageCode),
        trailing: trailing,
        onTap: () {
          adapter.locale = item;
          Navigator.of(context).pop();
        },
      );
    }).toList();
    return Container(child: Wrap(children: children));
  }
}
