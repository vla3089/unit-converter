import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:unit_converter/domain/area.dart';
import 'package:unit_converter/domain/speed.dart';
import 'package:unit_converter/domain/temperature.dart';
import 'package:unit_converter/domain/weight.dart';
import 'package:unit_converter/presentation/unit_row.dart';
import 'package:unit_converter/repo/dbprovider.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:unit_converter/repo/unit_row_model.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DashboardWidget extends StatefulWidget {
  final bool isInIditMode;

  const DashboardWidget({
    Key key,
    this.isInIditMode = false,
  }) : super(key: key);

  @override
  _DashboardWidgetState createState() => _DashboardWidgetState();
}

class _DashboardWidgetState extends State<DashboardWidget> {
  Box<UnitRowModel> _dashboardBox;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final provider = Provider.of<DBProvider>(context);
    provider.dashboardBox.then((value) {
      setState(() {
        _dashboardBox = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_dashboardBox == null) {
      return Center(
        child: CircularProgressIndicator(
          strokeWidth: 4,
        ),
      );
    } else {
      return ValueListenableBuilder(
        valueListenable: _dashboardBox.listenable(),
        builder: (BuildContext context, Box<UnitRowModel> box, Widget child) {
          final rows = box.values.toList();
          if (rows.isEmpty) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: SizedBox(
                      width: 300,
                      child: Text(
                        AppLocalizations.of(context).no_favorite,
                      ),
                    ),
                  ),
                  ElevatedButton.icon(
                    label: Text(
                        AppLocalizations.of(context).add_default_converters),
                    icon: Icon(Icons.add),
                    onPressed: () => _addDefaultConverters(),
                  )
                ],
              ),
            );
          }
          return ReorderableListView.builder(
              buildDefaultDragHandles: false,
              onReorder: (oldIndex, newIndex) {
                final item = rows[oldIndex];
                if (newIndex > oldIndex) {
                  newIndex -= 1;
                }
                rows.removeAt(oldIndex);
                rows.insert(newIndex, item);
                box.clear().then((value) => box.addAll(rows));
              },
              itemCount: rows.length,
              itemBuilder: (context, index) {
                final row = rows[index];
                return UnitRow(
                  key: Key(index.toString()),
                  index: index,
                  left: row.left,
                  right: row.right,
                  isInEditMode: widget.isInIditMode,
                  onDeletePressed: () {
                    box.deleteAt(index);
                  },
                );
              });
        },
      );
    }
  }

  _addDefaultConverters() async {
    _dashboardBox.addAll([
      UnitRowModel(Fahrenheit, Celsius),
      UnitRowModel(LbPound, Kg),
      UnitRowModel(Oz, Kg),
      UnitRowModel(SqF, SqM),
      UnitRowModel(KmPerHour, Mph),
    ]);
  }
}
