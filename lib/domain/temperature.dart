class Celsius {
  static final Celsius _singleton = Celsius._internal();

  factory Celsius() {
    return _singleton;
  }

  Celsius._internal();
}

class Fahrenheit {
  static final Fahrenheit _singleton = Fahrenheit._internal();

  factory Fahrenheit() {
    return _singleton;
  }

  Fahrenheit._internal();
}
