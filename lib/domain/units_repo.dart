import 'package:flutter/widgets.dart';
import 'package:unit_converter/domain/area.dart';
import 'package:unit_converter/domain/speed.dart';
import 'package:unit_converter/domain/unit_converter.dart';
import 'package:unit_converter/domain/temperature.dart';
import 'package:unit_converter/domain/weight.dart';

class UnitsRepo with ChangeNotifier {
  final _unitConverter = UnitConverter();

  // list of values use is woking on per type (Celsius, lb, kg etc.)
  final _values = Map<Type, double>();

  // map that groups related types so that change of one triggers change in others.
  final List<Set<Type>> _groups = [];

  Map<Type, double> get values => _values;

  UnitsRepo() {
    _groups.add({Celsius, Fahrenheit});
    _groups.add({Kg, G, Mg, Oz, LbPound});
    _groups.add({SqF, SqM});
    _groups.add({Mph, KmPerHour});
  }

  double getValueFor(Type type) {
    return _values[type];
  }

  void setValue(Type valueType, double value) {
    List<Set<Type>> valueGroups =
        _groups.where((element) => element.contains(valueType)).toList();
    valueGroups.forEach((group) {
      group.forEach((targetType) {
        if (value == null) {
          _values[targetType] = null;
        } else {
          _values[targetType] =
              _unitConverter.convert(valueType, targetType, value);
        }
      });
    });

    notifyListeners();
  }
}
