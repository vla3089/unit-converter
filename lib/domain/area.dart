class SqF {
  static final SqF _singleton = SqF._internal();
  factory SqF() {
    return _singleton;
  }
  SqF._internal();
}

class SqM {
  static final SqM _singleton = SqM._internal();
  factory SqM() {
    return _singleton;
  }
  SqM._internal();
}
