import 'package:unit_converter/domain/area.dart';
import 'package:unit_converter/domain/speed.dart';
import 'package:unit_converter/domain/temperature.dart';
import 'package:unit_converter/domain/weight.dart';

class GroupsModel {
  // map that groups related types so that change of one triggers change in others.
  final List<Set<Type>> _groups = [];

  List<Set<Type>> groups() => _groups;

  Set<Type> all() => _groups.fold(
      Set<Type>(), (Set<Type> acc, Set<Type> element) => acc.union(element));

  Set<Type> getMatchesFor(Type type) => _groups
      .where(
        (element) => element.contains(type),
      )
      .fold(
        Set<Type>(),
        (Set<Type> acc, Set<Type> element) => acc.union(element),
      );

  bool areInTheSameGroup(Type type1, Type type2) {
    final targetSet = {type1, type2};
    return groups().any((element) => element.containsAll(targetSet));
  }

  GroupsModel() {
    _groups.add({Celsius, Fahrenheit});
    _groups.add({Kg, G, Mg, Oz, LbPound});
    _groups.add({SqF, SqM});
    _groups.add({Mph, KmPerHour});
  }
}
