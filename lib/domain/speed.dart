class Mph {
  static final Mph _singleton = Mph._internal();
  factory Mph() {
    return _singleton;
  }
  Mph._internal();
}

class KmPerHour {
  static final KmPerHour _singleton = KmPerHour._internal();
  factory KmPerHour() {
    return _singleton;
  }
  KmPerHour._internal();
}
