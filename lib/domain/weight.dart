class Kg {
  static final Kg _singleton = Kg._internal();
  factory Kg() {
    return _singleton;
  }
  Kg._internal();
}

class G {
  static final G _singleton = G._internal();
  factory G() {
    return _singleton;
  }
  G._internal();
}

class Mg {
  static final Mg _singleton = Mg._internal();
  factory Mg() {
    return _singleton;
  }
  Mg._internal();
}

class Oz {
  static final Oz _singleton = Oz._internal();
  factory Oz() {
    return _singleton;
  }
  Oz._internal();
}

class LbPound {
  static final LbPound _singleton = LbPound._internal();
  factory LbPound() {
    return _singleton;
  }
  LbPound._internal();
}
