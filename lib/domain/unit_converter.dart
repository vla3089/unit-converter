import 'package:unit_converter/domain/area.dart';
import 'package:unit_converter/domain/speed.dart';
import 'package:unit_converter/domain/temperature.dart';
import 'package:unit_converter/domain/weight.dart';
import 'package:unit_converter/utils/pair.dart';

class UnitConverter {
  final _converters = Map<Pair<Type, Type>, PieceConverter>();

  // returns itself
  final PieceConverter _id = (value) => value;

  UnitConverter() {
    // temperature
    register(Celsius, Celsius, _id);
    register(Celsius, Fahrenheit, _celsToFahrehheight);
    register(Fahrenheit, Celsius, _fahrehheitToCels);
    register(Fahrenheit, Fahrenheit, _id);

    // Kg
    register(Kg, Kg, _id);
    register(Kg, G, (double value) => value * 1000);
    register(Kg, Mg, (double value) => value * 1E6);
    register(Kg, Oz, (double value) => value * 35.273962);
    register(Kg, LbPound, (double value) => value * 2.2046);

    // g
    register(G, Kg, (double value) => value * 1E-3);
    register(G, G, _id);
    register(G, Mg, (double value) => value * 1E3);
    register(G, Oz, (double value) => value * 1E-3 * 35.27396195);
    register(G, LbPound, (double value) => value * 1E-3 * 2.2046);

    // Mg
    register(Mg, Kg, (double value) => value * 1E-6);
    register(Mg, G, (double value) => value * 1E-3);
    register(Mg, Mg, _id);
    register(Mg, Oz, (double value) => value * 1E-6 * 35.27396195);
    register(Mg, LbPound, (double value) => value * 1E-6 * 2.2046);

    // Oz
    register(Oz, Kg, (double value) => value * 0.0283495231);
    register(Oz, G, (double value) => value * 0.0283495231 * 1E3);
    register(Oz, Mg, (double value) => value * 0.0283495231 * 1E6);
    register(Oz, Oz, _id);
    register(Oz, LbPound, (double value) => value * 0.0625);

    // LbPound
    register(LbPound, Kg, (double value) => value * 0.45359237);
    register(LbPound, G, (double value) => value * 0.45359237 * 1E3);
    register(LbPound, Mg, (double value) => value * 0.45359237 * 1E6);
    register(LbPound, Oz, (double value) => value * 16);
    register(LbPound, LbPound, _id);

    // Square feet
    register(SqF, SqF, _id);
    register(SqF, SqM, _sqFtTosqM);

    // Square meeters
    register(SqM, SqF, _sqMTosqFt);
    register(SqM, SqM, _id);

    // speed
    register(Mph, Mph, _id);
    register(Mph, KmPerHour, (double value) => value * 1.609344);
    register(KmPerHour, KmPerHour, _id);
    register(KmPerHour, Mph, (double value) => value * 0.6213711922);
  }

  void register(Type from, Type to, PieceConverter converter) {
    _converters[Pair(from, to)] = converter;
  }

  double convert(Type from, Type to, double value) =>
      _converters[Pair(from, to)](value);

  static double _celsToFahrehheight(double c) => c * 1.8 + 32;

  static double _fahrehheitToCels(double f) => (f - 32) * 5 / 9;

  static double _sqFtTosqM(double sqFt) {
    return sqFt * 0.092903;
  }

  static double _sqMTosqFt(double sqM) {
    return sqM * 10.76391;
  }
}

typedef double PieceConverter(double value);
