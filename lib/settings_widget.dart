import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';
import 'package:unit_converter/presentation/app_language_row.dart';
import 'package:unit_converter/presentation/theme_mode_row.dart';
import 'package:unit_converter/repo/dbprovider.dart';
import 'package:unit_converter/repo/settings_adapter.dart';

class SettingsWidget extends StatefulWidget {
  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends State<SettingsWidget> {
  Box _settingsBox;
  Color _accentColor;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final provider = Provider.of<DBProvider>(context);
    provider.settingsBox.then((value) {
      setState(() {
        _settingsBox = value;
      });
    });
    _accentColor = Theme.of(context).accentColor;
  }

  @override
  Widget build(BuildContext context) {
    if (_settingsBox == null) {
      return Center(
        child: CircularProgressIndicator(
          strokeWidth: 4,
        ),
      );
    }

    return ValueListenableBuilder(
      valueListenable: _settingsBox.listenable(),
      builder: (context, Box box, widget) {
        final adapter = SettingsAdapter(box);
        return ListView(
          children: [
            ThemeModeRow(adapter: adapter, accentColor: _accentColor),
            AppLanguageRow(adapter: adapter, accentColor: _accentColor),
          ],
        );
      },
    );
  }
}
