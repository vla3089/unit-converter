import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:unit_converter/add_dashboard_viewmodel.dart';
import 'package:unit_converter/domain/groups_model.dart';
import 'package:unit_converter/presentation/select_unit_input_field.dart';
import 'package:unit_converter/presentation/titles_factory.dart';
import 'package:unit_converter/repo/dbprovider.dart';
import 'package:unit_converter/repo/unit_row_model.dart';

class AddDashboardWidget extends StatefulWidget {
  @override
  _AddDashboardWidgetState createState() => _AddDashboardWidgetState();
}

class _AddDashboardWidgetState extends State<AddDashboardWidget> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TitlesFactory titlesFactory;
  GroupsModel groupsModel;
  Box<UnitRowModel> _dashboardBox;
  AddDashboardViewModel vm;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    vm = AddDashboardViewModel(titlesFactory, groupsModel, context);
    final provider = context.read<DBProvider>();
    provider.dashboardBox.then((value) {
      setState(() {
        _dashboardBox = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    titlesFactory = context.read<TitlesFactory>();
    groupsModel = context.read<GroupsModel>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).title),
      ),
      body: Form(
        key: this._formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: SelectUnitInputField(
                      label: AppLocalizations.of(context).from,
                      suggestionsCallback: (pattern) =>
                          vm.getSuggestionsFrom(pattern),
                      onChanged: (value) {
                        setState(() {
                          vm.left = value;
                        });
                      },
                      validator: (value) {
                        return vm.validateLeft();
                      },
                    ),
                  ),
                  new Container(
                      margin: new EdgeInsets.symmetric(horizontal: 4.0),
                      child: new Icon(Icons.chevron_left)),
                  new Container(
                      margin: new EdgeInsets.symmetric(horizontal: 4.0),
                      child: new Icon(Icons.chevron_right)),
                  Expanded(
                    child: SelectUnitInputField(
                      label: AppLocalizations.of(context).to,
                      suggestionsCallback: (pattern) =>
                          vm.getSuggestionsTo(pattern),
                      onChanged: (value) {
                        setState(() {
                          vm.right = value;
                        });
                      },
                      validator: (value) {
                        return vm.validateRight();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 40.0,
              ),
              ElevatedButton.icon(
                label: Text(AppLocalizations.of(context).add),
                icon: Icon(Icons.add),
                onPressed: () {
                  if (this._formKey.currentState.validate()) {
                    _dashboardBox
                        .add(UnitRowModel(vm.leftType(), vm.rightType()));
                    Navigator.of(context).pop();
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
