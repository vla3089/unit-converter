import 'package:flutter/material.dart';
import 'package:unit_converter/presentation/titles_factory.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'domain/groups_model.dart';

class AddDashboardViewModel {
  final TitlesFactory titlesFactory;
  final GroupsModel groupsModel;
  final BuildContext context;

  Map<String, Type> _titleToTypeMap = {};
  List<String> _keysList;

  String left;
  String right;

  Type leftType() => _titleToTypeMap[left];

  Type rightType() => _titleToTypeMap[right];

  AddDashboardViewModel(
    this.titlesFactory,
    this.groupsModel,
    this.context,
  ) {
    groupsModel.all().forEach((element) {
      _titleToTypeMap[titlesFactory.getTitle(context, element)] = element;
    });

    _keysList = _titleToTypeMap.keys.map((e) => e).toList();
    _keysList.sort();
  }

  String validateLeft() {
    if (!_titleToTypeMap.containsKey(left)) {
      return AppLocalizations.of(context).invalid_unit_type;
    }
    return null;
  }

  String validateRight() {
    if (!_titleToTypeMap.containsKey(right)) {
      return AppLocalizations.of(context).invalid_unit_type;
    }
    if (_titleToTypeMap.containsKey(left)) {
      Type leftType = _titleToTypeMap[left];
      Type rightType = _titleToTypeMap[right];
      if (!groupsModel.areInTheSameGroup(leftType, rightType)) {
        return AppLocalizations.of(context)
            .converter_pair_does_not_belong_same_group;
      }
    }
    return null;
  }

  List<String> getSuggestionsFrom(String pattern) {
    if (pattern.isEmpty) {
      return _keysList;
    } else {
      return _getKeysByPattern(pattern);
    }
  }

  List<String> getSuggestionsTo(String pattern) {
    if (left == null && pattern.isEmpty) {
      return _keysList;
    } else if (left != null && pattern.isEmpty) {
      if (_titleToTypeMap.containsKey(left)) {
        return _getTitlesForAGroup(_titleToTypeMap[left]);
      } else {
        return _keysList;
      }
    } else if (left == null && pattern.isNotEmpty) {
      return _getKeysByPattern(pattern);
    } else if (left != null && pattern.isNotEmpty) {
      if (_titleToTypeMap.containsKey(left)) {
        return _getKeysByPatternForAGroup(pattern, _titleToTypeMap[left]);
      } else {
        return _getKeysByPattern(pattern);
      }
    }
    return _keysList;
  }

  List<String> _getByPattern(List<String> data, String pattern) {
    return data
        .where(
            (element) => element.toLowerCase().contains(pattern.toLowerCase()))
        .toList();
  }

  List<String> _getKeysByPattern(String pattern) {
    return _getByPattern(_keysList, pattern);
  }

  List<String> _getKeysByPatternForAGroup(String pattern, Type groupMember) {
    return _getByPattern(_getTitlesForAGroup(groupMember), pattern);
  }

  List<String> _getTitlesForAGroup(Type groupMember) {
    return groupsModel
        .getMatchesFor(groupMember)
        .map((e) => titlesFactory.getTitle(context, e))
        .toList();
  }
}
