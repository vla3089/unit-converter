import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';
import 'package:unit_converter/add_dashboard_widget.dart';
import 'package:unit_converter/dashboard_widget.dart';
import 'package:unit_converter/domain/groups_model.dart';
import 'package:unit_converter/domain/units_repo.dart';
import 'package:unit_converter/presentation/titles_factory.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:unit_converter/repo/dbprovider.dart';
import 'package:unit_converter/repo/locale_adapter.dart';
import 'package:unit_converter/repo/settings_adapter.dart';
import 'package:unit_converter/repo/theme_mode_adapter.dart';
import 'package:unit_converter/repo/unit_row_adapter.dart';
import 'package:unit_converter/settings_widget.dart';

void main() {
  Hive.registerAdapter(UnitRowAdapter());
  Hive.registerAdapter(LocaleAdapter());
  Hive.registerAdapter(ThemeModeAdapter());

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Box _settingsBox;

  @override
  void initState() {
    super.initState();
    Hive.initFlutter().then((value) => Hive.openBox("settings")).then((value) {
      setState(() {
        _settingsBox = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_settingsBox == null) {
      return Center(
        child: CircularProgressIndicator(
          strokeWidth: 4,
        ),
      );
    }

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UnitsRepo()),
        ChangeNotifierProvider(create: (_) => TitlesFactory()),
        Provider.value(value: DBProvider()),
        Provider.value(value: GroupsModel()),
      ],
      child: ValueListenableBuilder(
        valueListenable: _settingsBox.listenable(),
        builder: (context, Box box, widget) {
          final adapter = SettingsAdapter(box);
          return MaterialApp(
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            locale: adapter.locale,
            onGenerateTitle: (BuildContext context) =>
                AppLocalizations.of(context).title,
            theme: ThemeData(
              brightness: Brightness.light,
              primarySwatch: Colors.teal,
            ),
            darkTheme: ThemeData(
              brightness: Brightness.dark,
              primarySwatch: Colors.teal,
            ),
            themeMode: adapter.themeMode,
            initialRoute: "/dashboard",
            routes: {
              "/dashboard": (context) => MyHomePage(),
              "/dashboard/add": (context) => AddDashboardWidget(),
            },
          );
        },
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _isInIditMode = false;
  int _selectedIndex = 0;

  static List<Widget> _widgetOptions = <Widget>[
    null,
    SettingsWidget(),
  ];

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).title),
        brightness: Brightness.dark,
        actions: _getToolbarActions(),
      ),
      body: _getBody(),
      floatingActionButton: _getFab(),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: AppLocalizations.of(context).favorites,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: AppLocalizations.of(context).settings,
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget _getBody() {
    if (_selectedIndex == 0) {
      return DashboardWidget(
        isInIditMode: _isInIditMode,
      );
    }
    return _widgetOptions[_selectedIndex];
  }

  Widget _getFab() {
    if (_selectedIndex == 0 && !_isInIditMode) {
      return FloatingActionButton.extended(
        onPressed: () {
          Navigator.pushNamed(context, "/dashboard/add");
        },
        icon: Icon(Icons.add),
        label: Text(AppLocalizations.of(context).add),
      );
    }
    return null;
  }

  List<Widget> _getToolbarActions() {
    if (_selectedIndex == 0) {
      Icon toggleEditModeIcon;
      if (_isInIditMode) {
        toggleEditModeIcon = Icon(Icons.save);
      } else {
        toggleEditModeIcon = Icon(Icons.edit);
      }

      return <Widget>[
        IconButton(
          icon: toggleEditModeIcon,
          onPressed: () => setState(() {
            _isInIditMode = !_isInIditMode;
          }),
        )
      ];
    }
    return [];
  }
}
