class Pair<A, B> {
  final A first;
  final B second;

  Pair(this.first, this.second);

  bool operator ==(other) =>
      other is Pair<A, B> && first == other.first && second == other.second;

  int get hashCode => first.hashCode + second.hashCode;
}

typedef Converter<T> = double Function(T a, T b);
