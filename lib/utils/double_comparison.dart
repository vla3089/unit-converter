double epsilon = 0.001;

/* Compare two {@code double} values
 * @param a some <i>double</i> value
 * @param b some other <i>double</i> value
 * @return {@code true} if the two values are equal
 */
bool equals(double a, double b) {
  if (a == b) return true;
  return (a - b).abs() < epsilon;
}

/*
 * Compare {@code a} and {@code b}
 * @param a some <i>double</i> value
 * @param b some other <i>double</i> value
 * @return a negative value if {@code a} is smaller than {@code b},
 * a positive value if {@code a} is larger than {@code b}, else {code 0}.
*/
int compare(final double a, final double b) {
  return equals(a, b)
      ? 0
      : (a < b)
          ? -1
          : 1;
}
