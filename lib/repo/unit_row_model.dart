import 'package:hive/hive.dart';

class UnitRowModel extends HiveObject {
  Type left;
  Type right;

  UnitRowModel(this.left, this.right);

  bool operator ==(other) =>
      other is UnitRowModel && left == other.left && right == other.right;

  int get hashCode => left.hashCode + right.hashCode;
}
