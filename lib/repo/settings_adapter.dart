import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';

class SettingsAdapter {
  final Box _settingsBox;

  SettingsAdapter(this._settingsBox);

  String get themeModeKey => "themeMode";

  String get localeKey => "locale";

  ThemeMode get themeMode =>
      _settingsBox.get(themeModeKey, defaultValue: ThemeMode.system);
  set themeMode(ThemeMode value) {
    _settingsBox.put(themeModeKey, value);
  }

  // returns nullable, null means no locale set
  Locale get locale => _settingsBox.get(localeKey);
  set locale(Locale value) {
    _settingsBox.put(localeKey, value);
  }
}
