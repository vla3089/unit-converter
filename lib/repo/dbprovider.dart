import 'package:hive/hive.dart';
import 'package:unit_converter/repo/unit_row_model.dart';

class DBProvider {
  static final DBProvider _singleton = DBProvider._internal();
  factory DBProvider() {
    return _singleton;
  }

  Future<Box<UnitRowModel>> dashboardBox;

  Future<Box> settingsBox;

  DBProvider._internal() {
    dashboardBox = Hive.openBox<UnitRowModel>("dashboard");
    settingsBox = Hive.openBox("settings");
  }
}
