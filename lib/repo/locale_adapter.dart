import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';

class LocaleAdapter extends TypeAdapter<Locale> {
  @override
  Locale read(BinaryReader reader) {
    return Locale.fromSubtags(languageCode: reader.readString());
  }

  @override
  final typeId = 2;

  @override
  void write(BinaryWriter writer, Locale obj) {
    writer.writeString(obj.languageCode);
  }
}
