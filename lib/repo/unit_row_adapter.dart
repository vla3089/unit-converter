import 'package:hive/hive.dart';
import 'package:unit_converter/domain/area.dart';
import 'package:unit_converter/domain/speed.dart';
import 'package:unit_converter/domain/temperature.dart';
import 'package:unit_converter/domain/weight.dart';
import 'package:unit_converter/repo/unit_row_model.dart';

class UnitRowAdapter extends TypeAdapter<UnitRowModel> {
  // items order is important
  // don't reorder or insert items
  // if add - add to the end
  final List<Type> _values = [
    SqF,
    SqM,
    Celsius,
    Fahrenheit,
    Kg,
    G,
    Mg,
    Oz,
    LbPound,
    Mph,
    KmPerHour
  ];

  @override
  UnitRowModel read(BinaryReader reader) {
    Type left = _values[reader.readInt()];
    Type right = _values[reader.readInt()];
    return UnitRowModel(left, right);
  }

  @override
  final typeId = 1;

  @override
  void write(BinaryWriter writer, UnitRowModel obj) {
    writer.writeInt(_getIndexOrThrow(obj.left));
    writer.writeInt(_getIndexOrThrow(obj.right));
  }

  int _getIndexOrThrow(Type type) {
    int index = _values.indexOf(type);
    if (index == -1) {
      throw ArgumentError.value(type, "type", "Not supported type $type");
    }

    return index;
  }
}
